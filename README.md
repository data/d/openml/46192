# OpenML dataset: Covid19-global

https://www.openml.org/d/46192

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Daily values of confirmed cases, deaths and recovers for COVID-19 in several countries.

From original source:
-----
MThis folder contains daily time series summary tables, including confirmed, deaths and recovered. All data is read in from the daily case report. The time series tables are subject to be updated if inaccuracies are identified in our historical data.

Two time series tables are for the US confirmed cases and deaths, reported at the county level. They are named time_series_covid19_confirmed_US.csv, time_series_covid19_deaths_US.csv, respectively.

Three time series tables are for the global confirmed cases, recovered cases and deaths. Australia, Canada and China are reported at the province/state level. Dependencies of the Netherlands, the UK, France and Denmark are listed under the province/state level. The US and other countries are at the country level. The tables are renamed time_series_covid19_confirmed_global.csv and time_series_covid19_deaths_global.csv, and time_series_covid19_recovered_global.csv, respectively.
-----

We have joined the confirmed, deaths and recovered datasets to create multivariate series. 

Preprocessing:

1 - For the 'confirmed' and 'deaths' datasets, we have grouped the values for the 'Country/Region' 'Canada' for all the 'Province/State'.

The 'recovered' dataset does not have the several 'Province/State' for 'Canada', only the country, so we grouped in order to merge all the datasets.

2 - Filled NaN values for 'Province/State' with the value 'Country'.

3 - Filled NaN values for 'Lat' and 'Long' with 0.0.

4 - Melted the datasets with identifiers 'Province/State', 'Country/Region', 'Lat', 'Long', obtaining columns 'date' and 'value_X', where X is 0 for confirmed cases, 1 for deaths and 2 for recoveries.

5 - Standardize the date to the format %Y-%m-%d.

6 - Merged all the datasets.

7 - Created column 'id_series' from 'Province/State', 'Country/Region' with index from 0 to 273.

8 - Renamed columns 'Province/State', 'Country/Region', 'Lat', 'Long' to 'covariate_0', 'covariate_1', 'covariate_2', 'covariate_3'.

9 - Created column 'time_step' with increasing values of the time_step for the time series.

10 - Casted 'value_X' columns to int, defined 'id_series', covariate_0' and 'covariate_1' as 'category' and casted 'covariate_2' and 'covariate_3' to float.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46192) of an [OpenML dataset](https://www.openml.org/d/46192). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46192/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46192/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46192/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

